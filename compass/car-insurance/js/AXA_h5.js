;(function($, window) {
    "use strict";

    //城市切换文案变换
    $("#selectCity").change(function() {
        if ($(this).val() == "shanghai") {
            $(".nanjing-part").hide();
            $(".shanghai-part").show();
        }
        else if ($(this).val() == "nanjing") {
            $(".shanghai-part").hide();
            $(".nanjing-part").show();
        }
    });

    function selectNJ() {
        $("#selectCity option[value='nanjing']")[0].selected = true;
        $("#selectCity").trigger("change");
    }
    function selectSH() {
        $("#selectCity option[value='shanghai']")[0].selected = true;
        $("#selectCity").trigger("change");
    }

    //纬度、经度
    var latitude, longitude;
    var latLon;
    window.onload = getLocation();

    function getLocation(){
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showPosition,showError);
        }else{
            alert("浏览器不支持地理定位。");
        }
    }

    function showPosition(position) {
        latitude = position.coords.latitude; //纬度
        longitude = position.coords.longitude; //经度
        // alert('纬度:'+latitude+',经度:'+longitude);

        latLon = latitude + "," + longitude;
        //google
        var url = 'http://maps.google.cn/maps/api/geocode/json?latlng='+latLon+'&language=CN';
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function(){
                // $("#google_geo").html('正在定位...');
            },
            success: function (json) {
                if(json.status=='OK') {
                    // console.log(json.results);
                    // alert("google:" + json.results[0].formatted_address);
                    if (json.results[0].formatted_address.indexOf("江苏省") >= 0) {
                        selectNJ();
                    }
                    else {
                        selectSH();
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // $("#google_geo").html(latLon+"地址位置获取失败");
            }
        });
    }

    function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                alert("定位失败,用户拒绝请求地理定位");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("定位失败,位置信息是不可用");
                break;
            case error.TIMEOUT:
                alert("定位失败,请求获取用户位置超时")
                break;;
            case error.UNKNOWN_ERROR:
                alert("定位失败,定位系统失效");
                break;
        }
    }

})(Zepto, window);