;(function($, window) {
    "use strict";

    //验证手机号
    var regPhone = /^1\d{10}$/;
    $(".tel").on("change input", function() {
        if (regPhone.test($(this).val())) {
            $(".get-code").removeClass("disabled");
        }
        else
            $(".get-code").addClass("disabled");
    });

    $(".get-code").on("tap", function() {
        timerStart();

        //获取验证码
        /////////////
        /////////////
        /////////////////

        $(".notice").css("visibility", "visible");
        // $(".notice").css("visibility", "hidden");
    });

    //获取验证码倒计时
    function timerStart() {
        var seconds = 60;
        var btn = $(".get-code");
        btn.addClass("disabled").text(seconds + "秒后可重发");
        var timer = window.setInterval(function() {
            seconds--;
            btn.addClass("disabled").text(seconds + "秒后可重发");
            if (seconds <= 0) {
                btn.removeClass("disabled").text("获取验证码");
                window.clearInterval(timer);
            }
        }, 1000);
    }

})(Zepto, window);